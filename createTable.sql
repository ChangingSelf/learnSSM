CREATE DATABASE IF NOT EXISTS test;

USE test;

# 删除表
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS student_course;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS grade;
DROP TABLE IF EXISTS course;

# 创建课程表
create table course
(	
	cid INT AUTO_INCREMENT primary key,
	cname char(30),
	cdesc char(100),
	deleted INT DEFAULT 0
);

# 创建班级表
CREATE TABLE grade
(
	gid INT AUTO_INCREMENT primary key, 
	gname char(30),
	deleted INT DEFAULT 0
);


# 创建学生信息表
create table student
(	
	sid INT AUTO_INCREMENT primary key,
	snum char(30),# 学号
	sname char(30),
	sex char(30),
	age char(30),
	address char(30),
	remark char(100),# 备注
	deleted INT DEFAULT 0,
	gid INT,
	FOREIGN KEY(gid) REFERENCES grade(gid)
);

# 创建选课表
CREATE TABLE student_course
(
	sid INT,
	cid INT,
	score INT,
	deleted INT DEFAULT 0,
	PRIMARY KEY(sid,cid),
	FOREIGN KEY(sid) REFERENCES student(sid),
	FOREIGN KEY(cid) REFERENCES course(cid)
);


# 创建用户表
CREATE TABLE account
(
	uid INT AUTO_INCREMENT PRIMARY KEY,
	username CHAR(30),
	`password` CHAR(32),
	deleted INT,
	stat INT DEFAULT 0
);



INSERT INTO course(cname,cdesc)
VALUES
('软件工程','软件工程'),
('软件详细设计',''),
('软件需求分析',''),
('测试课程1',''),
('测试课程2',''),
('测试课程3',''),
('测试课程4',''),
('测试课程5',''),
('测试课程6',''),
('测试课程7',''),
('测试课程8',''),
('测试课程9',''),
('测试课程10',''),
('测试课程11',''),
('测试课程12',''),
('测试课程13',''),
('测试课程14',''),
('测试课程15',''),
('数据库','');



INSERT INTO grade(gname)
VALUES
('软件工程六班'),
('软件工程七班');

insert
into student(snum,sname,gid,sex,age,address)
values
('2017901006','张三',1,'男',22,'北京'),
('2017901007','李四',2,'女',23,'德玛西亚'),
('2017901008','王五',1,'女',24,'上海'),
('2017901009','赵六',2,'男',21,'西安'),
('2017901010','张三',1,'男',22,'北京'),
('2017901011','张三',1,'男',22,'北京'),
('2017901012','张三',1,'男',22,'北京'),
('2017901013','张三',1,'男',22,'北京'),
('2017901014','张三',1,'男',22,'北京'),
('2017901015','张三',1,'男',22,'北京'),
('2017901016','张三',1,'男',22,'北京'),
('2017901017','张三',1,'男',22,'北京'),
('2017901018','张三',1,'男',22,'北京'),
('2017901019','张三',1,'男',22,'北京'),
('2017901020','张三',1,'男',22,'北京'),
('2017901021','张三',1,'男',22,'北京'),
('2017901022','张三',1,'男',22,'北京'),
('2017901023','张三',1,'男',22,'北京'),
('2017901024','张三',1,'男',22,'北京'),
('2017901025','张三',1,'男',22,'北京'),
('2017901026','张三',1,'男',22,'北京');

# 测试密码均与用户名相同
INSERT INTO account(username,`password`)
VALUES
('admin','75bd3b5c890813b46150b93bb175fb4e'),
('root','159f6990bb1e1277f90f57e0206af9a4');
