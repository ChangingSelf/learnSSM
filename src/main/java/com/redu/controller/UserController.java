package com.redu.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.redu.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping("/user/login")
	public String login(String username,String password){
		//1.将登录的用户名和密码封装到shiro的令牌类中

		password = new Md5Hash(password,username,1000).toString();
		UsernamePasswordToken token = new UsernamePasswordToken(username,password);
		System.out.println(password);
		//2.根据抛出的异常来返回值

		Subject subject=SecurityUtils.getSubject();
		if(!subject.isAuthenticated()){
			//如果主体没有认证通过
			try {
				subject.login( token );//会跳转到自定义的MyRealm类中
				
			} catch ( UnknownAccountException uae ) {
				//用户名输入不正确
				System.out.println("用户名输入不正确");
				return "-1";
			} catch ( IncorrectCredentialsException ice ) {
				System.out.println("密码输入错误");
				return "-2";
			} 

		}



		return "0";//登录成功

	}
}
