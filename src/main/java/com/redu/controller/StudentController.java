package com.redu.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.redu.bean.Course;
import com.redu.bean.Grade;
import com.redu.bean.Student;
import com.redu.service.StudentService;
import com.redu.service.impl.CourseServiceImpl;
import com.redu.service.impl.StudentServiceImpl;

@RestController
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	/**
	 * 查询课程
	 * @param page 当前显示的页
	 * @param rows 每一页显示的记录数
	 * @param sname 姓名 
	 * @param sex 性别
	 * @return
	 */
	@RequestMapping("/listPageStudent")
	public String listPageStudent(int page,int rows,String sname,String sex){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("offset", (page-1)*rows);
		map.put("size",rows);
		map.put("sname", sname);
		map.put("sex", sex);
		Map<String,Object>result=new HashMap<String,Object>();
		result.put("total", studentService.getStudentCount(map));
		result.put("rows", studentService.listPageStudent(map));
		return new Gson().toJson(result);
	}
	@RequestMapping("/listStudent")
	public String listStudent(String sname,String sex){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("sname", sname);
		map.put("sex", sex);
		//此方法给下拉框使用，不需要按照表格所需格式返回
		return new Gson().toJson(studentService.listStudent(map));
	}
	
	
	@RequestMapping("/insertStudent")
	public boolean insertStudent(Student student,int gid){
		Grade grade = new Grade();
		grade.setGid(gid);
		student.setGrade(grade);
		return studentService.insertStudent(student);
	}
	
	@RequestMapping("/deleteStudent")
	public boolean deleteStudent(@RequestParam("sids[]") String[] sids){
		return studentService.deleteStudent(sids);
	}
	
	@RequestMapping("/updateStudent")
	public boolean updateStudent(Student student,int gid){
		Grade grade = new Grade();
		grade.setGid(gid);
		student.setGrade(grade);
		return studentService.updateStudent(student);
	}

}
