package com.redu.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.redu.bean.Course;
import com.redu.service.CourseService;
import com.redu.service.impl.CourseServiceImpl;

@RestController
@RequestMapping("/course")
public class CourseController {
	@Autowired
	private CourseService courseService;//注入
	/**
	 * 查询课程
	 * @param page 当前显示的页
	 * @param rows 每一页显示的记录数
	 * @param cname 课程名
	 * @return
	 */
	@RequestMapping("/listPageCourse")
	public String listPageCourse(int page,int rows,String cname){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("offset", (page-1)*rows);
		map.put("size",rows);
		map.put("cname", cname);
		Map<String,Object>result=new HashMap<String,Object>();
		result.put("total", courseService.getCourseCount(map));
		result.put("rows", courseService.listPageCourse(map));
		return new Gson().toJson(result);
	}
	
	
	@RequestMapping("/insertCourse")
	public boolean insertCourse(Course course){
		return courseService.insertCourse(course);
	}
	
	@RequestMapping("/deleteCourse")
	public boolean deleteCourse(@RequestParam("cids[]") String[] cids){
		return courseService.deleteCourse(cids);
	}
	
	@RequestMapping("/updateCourse")
	public boolean updateCourse(Course course){
		return courseService.updateCourse(course);
	}

}
