package com.redu.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.redu.bean.Grade;
import com.redu.service.GradeService;

@RestController
@RequestMapping("/grade")
public class GradeController {
	
	@Autowired
	private GradeService gradeService;
	
	
	@RequestMapping("/listGrade")
	public List<Grade> listGrade(){
		return gradeService.listGrade();
	}
	
	@RequestMapping("/listPage")
	public String listPageStudent(int page,int rows,String gname){
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("offset", (page-1)*rows);
		map.put("size",rows);
		map.put("gname", gname);
		Map<String,Object>result=new HashMap<String,Object>();
		result.put("total", gradeService.getCount(map));
		result.put("rows", gradeService.listPage(map));
		return new Gson().toJson(result);
	}
	
	
	
}
