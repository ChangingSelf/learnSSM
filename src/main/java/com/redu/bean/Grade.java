package com.redu.bean;

import java.io.Serializable;
import java.util.*;

public class Grade implements Serializable{
	private Integer gid;
	private String gname;
	private Integer deleted;
	
	
	
	public Integer getGid() {
		return gid;
	}
	public void setGid(Integer gid) {
		this.gid = gid;
	}
	public String getGname() {
		return gname;
	}
	public void setGname(String gname) {
		this.gname = gname;
	}
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	
}
