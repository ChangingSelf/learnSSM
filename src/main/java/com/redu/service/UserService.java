package com.redu.service;

import com.redu.bean.User;

public interface UserService {
	User login(String username);
}
