package com.redu.service;

import java.util.List;
import java.util.Map;

import com.redu.bean.Course;

public interface CourseService {
	List<Course> listPageCourse(Map<String,Object> map);
	
	int getCourseCount(Map<String,Object> map);
	
	boolean deleteCourse(String[] cids);

	boolean updateCourse(Course course);
	
	boolean insertCourse(Course course);
}
