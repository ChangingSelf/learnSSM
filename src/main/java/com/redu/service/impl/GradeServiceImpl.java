package com.redu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.redu.bean.Grade;
import com.redu.dao.GradeMapper;
import com.redu.service.GradeService;

@Service
public class GradeServiceImpl implements GradeService {

	@Autowired
	private GradeMapper gradeMapper;
	
	@Override
	public List<Grade> listGrade() {
		return gradeMapper.listGrade();
	}

	@Override
	public List<Grade> listPage(Map<String, Object> map) {
		return gradeMapper.listPage(map);
	}

	@Override
	public int getCount(Map<String, Object> map) {
		return gradeMapper.getCount(map);
	}


}
