package com.redu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.redu.bean.Student;
import com.redu.dao.StudentMapper;
import com.redu.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	@Autowired
	private StudentMapper studentMapper;


	@Override
	public List<Student> listPageStudent(Map<String, Object> map) {
		return studentMapper.listPageStudent(map);
	}

	@Override
	public boolean insertStudent(Student student) {
		return studentMapper.insert(student)>0;
	}

	@Override
	public boolean deleteStudent(String[] sids) {
		return studentMapper.delete(sids)>0;
	}

	@Override
	public boolean updateStudent(Student student) {
		return studentMapper.update(student)>0;
	}

	@Override
	public int getStudentCount(Map<String, Object> map) {
		return studentMapper.getStudentCount(map);
	}

	@Override
	public List<Student> listStudent(Map<String, Object> map) {
		return studentMapper.listStudent(map);
	}
	
	
}
