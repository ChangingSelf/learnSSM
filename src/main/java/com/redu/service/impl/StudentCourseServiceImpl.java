package com.redu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.redu.bean.StudentCourse;
import com.redu.dao.StudentCourseMapper;
import com.redu.service.StudentCourseService;

@Service
public class StudentCourseServiceImpl implements StudentCourseService {

	@Autowired
	private StudentCourseMapper studentCourseMapper;
	
	
	@Override
	public boolean chooseCourse(StudentCourse courseChoosing) {
		return studentCourseMapper.chooseCourse(courseChoosing)>0;
	}


	@Override
	public List<StudentCourse> listPage(Map<String, Object> map) {
		return studentCourseMapper.listPage(map);
	}


	@Override
	public int getCount(Map<String, Object> map) {
		return studentCourseMapper.getCount(map);
	}


	@Override
	public boolean marking(StudentCourse studentCourse) {
		return studentCourseMapper.marking(studentCourse)>0;
	}


	@Override
	public boolean delete(StudentCourse[] studentCourses) {
		if(studentCourses==null) return false;
		//如果删除的数目和原本的数目不等，则返回false
		return studentCourseMapper.delete(studentCourses)==studentCourses.length;
	}
	
	

}
