package com.redu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.redu.bean.Course;
import com.redu.dao.CourseMapper;
import com.redu.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
	
	@Autowired
	private CourseMapper courseMapper;

	@Override
	public List<Course> listPageCourse(Map<String, Object> map) {
		return courseMapper.listPageCourse(map);
	}

	@Override
	public int getCourseCount(Map<String, Object> map) {
		return courseMapper.getCourseCount(map);
	}

	@Override
	public boolean deleteCourse(String[] cids) {
		return courseMapper.deleteCourse(cids)>0;
	}

	@Override
	public boolean updateCourse(Course course) {
		return courseMapper.updateCourse(course)>0;
	}

	@Override
	public boolean insertCourse(Course course) {
		return courseMapper.insertCourse(course)>0;
	}
	
	
	
}
