package com.redu.service;

import java.util.List;
import java.util.Map;

import com.redu.bean.Grade;

public interface GradeService {
	List<Grade> listGrade();
	
	List<Grade> listPage(Map<String, Object> map);
	
	int getCount(Map<String,Object> map);
}
