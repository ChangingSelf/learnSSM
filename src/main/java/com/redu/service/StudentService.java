package com.redu.service;

import java.util.List;
import java.util.Map;

import com.redu.bean.Student;

public interface StudentService {
	
	List<Student> listPageStudent(Map<String,Object> map);
	
	List<Student> listStudent(Map<String,Object> map);
	
	boolean insertStudent(Student student);
	
	boolean deleteStudent(String[] sids);
	
	boolean updateStudent(Student student);
	
	int getStudentCount(Map<String, Object> map);
}
