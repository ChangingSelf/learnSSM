package com.redu.service;

import java.util.List;
import java.util.Map;

import com.redu.bean.StudentCourse;

public interface StudentCourseService {
	boolean chooseCourse(StudentCourse studentCourse);
	
	List<StudentCourse> listPage(Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	boolean marking(StudentCourse studentCourse);
	
	boolean delete(StudentCourse[] studentCourses);
}
