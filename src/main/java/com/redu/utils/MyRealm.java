package com.redu.utils;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.tomcat.util.descriptor.web.LoginConfig;
import org.springframework.beans.factory.annotation.Autowired;

import com.redu.bean.User;
import com.redu.service.UserService;

/**
 * 定义用户认证或者授权的类
 * @author ChangingSelf
 *
 */
public class MyRealm extends AuthorizingRealm{

	@Autowired
	private UserService userService;

	/**
	 * 处理用户登录
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		String username = (String)token.getPrincipal();
		User user=login(username);
		//将登录后的信息封装进去，返回去匹配密码
		SimpleAuthenticationInfo info=null;
		if(user!=null){
			//如果能查到此用户
			//其中getName()方法是获取当前认证类的名称
			info=new SimpleAuthenticationInfo(user.getUsername(),user.getPassword(),getName());
			
		}
		return info;
	}
	
	/**
	 * 调用业务层去查数据库
	 * @param username
	 * @return
	 */
	private User login(String username){
		return userService.login(username);
	}
	
	
	/**
	 * 处理权限
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		return null;
	}
}
