package com.redu.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.redu.bean.Student;
@Repository
public interface StudentMapper {

	int insert(Student student);
	
	int update(Student student);
	
	int delete(String[] ids);
	
	/**
	 * mybatis方法传递多参数时，必须使用@Param注解声明参数名称，否则报参数传递个数错误。
	 * 或者封装到Map集合中
	 * @param sname
	 * @param sex
	 * @return
	 */
	List<Student> select(@Param("sname") String sname,@Param("sex") String sex);
	
	/**
	 * mybatis方法传递多参数时，必须使用@Param注解声明参数名称，否则报参数传递个数错误。
	 * 或者封装到Map集合中
	 * @param sname
	 * @param sex
	 * @param offset
	 * @param size
	 * @return
	 */
	List<Student> listPageStudent(Map<String,Object> map);
	
	List<Student> listStudent(Map<String,Object> map);
	
	int getStudentCount(Map<String,Object> map);
	
}
