package com.redu.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.redu.bean.Course;
@Repository
public interface CourseMapper {
	List<Course> listPageCourse(Map<String,Object> map);
	
	int getCourseCount(Map<String,Object> map);
	
	int deleteCourse(String[] cids);
	
	int insertCourse(Course course);
	
	int updateCourse(Course course);
}
