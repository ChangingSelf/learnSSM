package com.redu.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.redu.bean.*;
@Repository
public interface GradeMapper {
	List<Grade> listGrade();
	List<Grade> listPage(Map<String,Object> map);
	int getCount(Map<String,Object> map);
}
