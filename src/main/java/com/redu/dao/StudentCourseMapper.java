package com.redu.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.redu.bean.Student;
import com.redu.bean.StudentCourse;
@Repository
public interface StudentCourseMapper {
	//添加选课
	int chooseCourse(StudentCourse studentCourse);
	//查询
	List<StudentCourse> listPage(Map<String, Object> map);
	//获得记录数
	int getCount(Map<String, Object> map);
	
	//登记成绩
	int marking(StudentCourse studentCourse);
	
	//删除
	int delete(StudentCourse[] studentCourses);
}
