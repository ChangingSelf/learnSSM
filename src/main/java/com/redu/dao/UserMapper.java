package com.redu.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.redu.bean.User;

@Repository
public interface UserMapper {
	User login(@Param("username") String username);
}
