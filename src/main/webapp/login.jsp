<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录</title>
<link rel="stylesheet" type="text/css"
	href="jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
<script type="text/javascript" src="jeasyui/jquery.min.js"></script>
<script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>

</head>
<body>
	<script type="text/javascript">
	$(function(){
		$('#login-dialog').dialog({
			buttons:[{
				text:'登录',
				iconCls:'icon-man',
				handler:function(){
					$('#login-form').form('submit', {   
					    url:'user/login',   
					    onSubmit: function(){   
					        return $('#login-dialog').form('validate');
					    },   
					    success:function(flag){ 
					        switch(flag){
					        case "0"://登录成功
					        	window.location.href="index.jsp";
					        	
					        	break;
					        case "-1"://用户名输入不正确
					        	alert('用户名输入不正确');
					        	break;
					        case "-2"://密码输入不正确
					        	alert('密码输入不正确');
					        	break;
					        default:
					        	
					        	break;
					        }
					    }   
					});  
				}
				
			}]
		})
		
		
	})
	
</script>
	<div id="login-dialog" class="easyui-dialog" title="登录"
		style="width: auto; height: auto;"
		data-options="iconCls:'icon-save',resizable:true,modal:true,closable:false">
		<form id="login-form" method="post">
			<table>
				<tr>

					<td>用户名</td>
					<td><input name="username" class="easyui-validatebox"
						data-options="required:true" /></td>
				</tr>
				<tr>
					<td>密码</td>
					<td><input type="password" name="password" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>

			</table>
		</form>


	</div>
</body>
</html>