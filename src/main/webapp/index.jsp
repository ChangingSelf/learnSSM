<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
<script type="text/javascript" src="jeasyui/jquery.min.js"></script>
<script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
<script>
function openTabs(text, url) {
	if ($("#tabs").tabs('exists', text)) {
		$("#tabs").tabs('select', text)
	} else {
		var myContext = "<iframe frameborder='0' scrolling='auto' style='width:100%;height:100%' src="
				+ url + "></iframe>";
		$("#tabs").tabs('add', {
			title : text,
			closable : true,
			content : myContext
		})
	}
}

</script>


</head>
<body>
<body class="easyui-layout">
	<div data-options="region:'north',title:'教务管理系统',split:true"
		style="height: 100px;">
		<div align="right">
		当前登录用户：【<shiro:principal/>】<a href="logout">注销</a>
		</div>
		
		</div>

	<!-- 导航菜单 -->
	<div data-options="region:'west',title:'导航菜单',split:true"
		style="width: 200px;">
		<div id="aa" class="easyui-accordion"
			style="width: 100%; height: 100%;">
			<div title="班级学生管理">
				<a href="#" style="width: 100%" onclick="openTabs('班级信息管理','jsp/gradelist.jsp')" class="easyui-linkbutton" data-options="plain:true">班级信息管理</a>
				<a href="#" style="width: 100%" onclick="openTabs('学生信息管理','jsp/studentList.jsp')" class="easyui-linkbutton" data-options="plain:true">学生信息管理</a>
			</div>
			<div title="课程信息管理">
				<a href="#" style="width: 100%" onclick="openTabs('课程信息管理','jsp/courseList.jsp')" class="easyui-linkbutton" data-options="plain:true">课程信息管理</a>
				<a href="#" style="width: 100%" onclick="openTabs('选课','jsp/selectCourse.jsp')" class="easyui-linkbutton" data-options="plain:true">选课</a>
				<a href="#" style="width: 100%" onclick="openTabs('选课信息管理','jsp/courseChoosingList.jsp')" class="easyui-linkbutton" data-options="plain:true">选课信息管理</a>
			</div>
			<div title="系统信息管理"></div>

		</div>

	</div>

	<!-- 数据显示 -->
	<div data-options="region:'center',title:'数据显示'"
		style="padding: 5px; background: #eee;">
		<div id="tabs" class="easyui-tabs" data-options="fit:true">
			<div title="首页" style="padding: 20px; display: none;">tab1</div>
		</div>
	</div>
</body>
</body>
</html>