<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="../jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../jeasyui/themes/icon.css">
<script type="text/javascript" src="../jeasyui/jquery.min.js"></script>
<script type="text/javascript" src="../jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../jeasyui/easyui-lang-zh_CN.js"></script>
</head>
<body>
	数据表格
	<script type="text/javascript">
	$(function(){
		//添加信息
		$('#btn-add').click(function(){
			$('#add-dialog').dialog({
				closed:false,
				buttons:[{
					text:'提交',
					iconCls:'icon-save',
					handler:function(){
						$('#add-form').form('submit', {   
						    url:'../course/insertCourse',   
						    onSubmit: function(){   
						        return $('#add-form').form('validate');
						    },   
						    success:function(flag){   
						        if(flag){
						        	$('#dg').datagrid('load');
						        	$('#add-dialog').dialog({
										closed:true
									});
						        }else{
						        	alert('操作失败');
						        }
						    }   
						});  
					}
				},{
					text:'重置',
					iconCls:'icon-redo',
					handler:function(){
						alert('reset');
					}
				}]
				
			})
			
		})
		//删除信息
		$('#btn-remove').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择要删除的数据');
			}else{
				const cids=[];
				
				for(var i in array){
					cids.push(array[i].cid);
				}
				console.log(cids);
				//jquery原生方法
				$.post('../course/deleteCourse',{
					'cids[]':cids
					},function(flag){
					if(flag){
						$('#dg').datagrid('reload');
					}else{
						alert('操作失败');
					}
				})
			}
		})
		//修改学生信息
		$('#btn-edit').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择要修改的数据');
			}else if(array.length!=1){
				alert('不能同时修改多条数据');
			}else{
				const course = $('#dg').datagrid('getSelected');
				console.log(course);
				$('#edit-form').form('load',{//加载本地记录
					cid:course.cid,
					cname:course.cname,
					cdesc:course.cdesc
				});
				$('#edit-dialog').dialog({
					closed:false,
					buttons:[{
						text:'提交',
						iconCls:'icon-save',
						handler:function(){
							$('#edit-form').form('submit', {   
							    url:'../course/updateCourse',   
							    onSubmit: function(){   
							        return $('edit-form').form('validate');
							    },   
							    success:function(flag){   
							        if(flag){
							        	$('#dg').datagrid('load');
							        	$('#edit-dialog').dialog({
											closed:true
										});
							        }else{
							        	alert('操作失败');
							        }
							    }   
							});
						}
					}]
					
				});

			}
		});
		
		$('#dg').datagrid({
			url : '../course/listPageCourse',
			fit: true,
			fitColumns:true,
			rownumbers:true,
			striped:true,
			pagination:true,
			pagePosition:'both',
			toolbar:'toolbar',
			columns : [ [ {
				field : 'cid',
				title : '课程号',
				width : 100,
				checkbox:true
			}, {
				field : 'cname',
				title : '课程名',
				width : 100
			}, {
				field : 'cdesc',
				title : '课程简介',
				width : 100
			} ] ]
		});

	})
	</script>
	
	<!-- 查询框 -->
	<table>
		<tr>
			<td>课程名</td>
			<td>
				<input id="cname" class="easyui-validatebox"/>
			</td>
			<td> 
				<a id="btn-search" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a> 
			</td>
		</tr>
		
	</table>
	<script type="text/javascript">
		$('#btn-search').click(function(){
			$('#dg').datagrid('load',{
				cname:$('#cname').val()
			})
		});
	</script>
	
	<!-- 工具条 -->
	<div id="toolbar">
		<a id="btn-add" href="#" class="easyui-linkbutton"
			data-options="iconCls:'icon-add'">添加</a> 
		<a id="btn-edit" href="#" class="easyui-linkbutton" 
			data-options="iconCls:'icon-edit'">修改</a>
		<a id="btn-remove" href="#" class="easyui-linkbutton"
			data-options="iconCls:'icon-remove'">删除</a>
	</div>
	<!-- 数据表格 -->
	<table id="dg"></table>
	<!-- 添加对话框 -->
	<div id="add-dialog" class="easyui-dialog" title="添加学生信息"
		style="width: 400px; height: 200px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="add-form" method="post">
			<table>
				<tr>
					<td>课程名</td>
					<td><input type="text" name="cname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>课程描述</td>
					<td><input type="text" name="cdesc" class="easyui-validatebox" /></td>

				</tr>

			</table>

		</form>
	</div>
	
	<!-- 修改对话框 -->
	<div id="edit-dialog" class="easyui-dialog" title="修改学生信息"
		style="width: 400px; height: 200px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="edit-form" method="post">
			<table>
				<tr>
				<input type="hidden" name="cid"/>
					<td>课程名</td>
					<td><input type="text" name="cname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>课程描述</td>
					<td><input type="text" name="cdesc" class="easyui-validatebox" /></td>

				</tr>

			</table>

		</form>
	</div>

</body>
</html>