<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="../jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../jeasyui/themes/icon.css">
<script type="text/javascript" src="../jeasyui/jquery.min.js"></script>
<script type="text/javascript" src="../jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../jeasyui/easyui-lang-zh_CN.js"></script>
</head>
<body>
	数据表格
	<script type="text/javascript">
	$(function(){
		
		//删除信息
		$('#btn-remove').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择要删除的数据');
			}else{
				const cids=[];
				const sids=[];
				
				for(var i in array){
					cids.push(array[i].course.cid);
					sids.push(array[i].student.sid);
				}
				console.log(array);
				console.log(cids);
				console.log(sids);
				//jquery原生方法
				$.post('../studentCourse/delete',{
					'sids[]':sids,
					'cids[]':cids
					},function(flag){
					if(flag){
						$('#dg').datagrid('reload');
					}else{
						alert('操作失败');
					}
				})
			}
		})

		//登记学生成绩
		$('#btn-marking').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择一条记录进行登记成绩');
			}else if(array.length!=1){
				alert('不能同时选择多条记录登记成绩');
			}else{
				const row = $('#dg').datagrid('getSelected');
				console.log(row);
				$('#marking-form').form('load',{//加载本地记录
					cid:row.course.cid,
					cname:row.course.cname,
					sid:row.student.sid,
					sname:row.student.sname
				});
				$('#marking-dialog').dialog({
					closed:false,
					buttons:[{
						text:'提交',
						iconCls:'icon-save',
						handler:function(){
							$('#marking-form').form('submit', {   
							    url:'../studentCourse/marking',   
							    onSubmit: function(){   
							        return $('marking-form').form('validate');
							    },   
							    success:function(flag){   
							        if(flag){
							        	$('#dg').datagrid('load');
							        	$('#marking-dialog').dialog({
											closed:true
										});
							        }else{
							        	alert('操作失败');
							        }
							    }   
							});
						}
					}]
					
				});

			}
		});
		
		$('#dg').datagrid({
			url : '../studentCourse/listPage',
			fit: true,
			fitColumns:true,
			rownumbers:true,
			striped:true,
			pagination:true,
			pagePosition:'both',
			toolbar:'toolbar',
			columns : [ [ {
				field : 'cid',
				title : '课程号',
				width : 100,
				checkbox:true
			}, {
				field : 'course',
				title : '课程名',
				width : 100,
				formatter: function(course,row,index){
					if (course){
						return course.cname;
					} else {
						return '';
					}
				}

			} , {
				field : 'student',
				title : '学生名',
				width : 100,
				formatter: function(student,row,index){
					if (student){
						return student.sname;
					} else {
						return '';
					}
				}
			} , {
				field : 'score',
				title : '成绩',
				width : 100
			} ] ]
		});

	})
	</script>

	
	
	<!-- 查询框 -->
	<table>
		<tr>
			<td>课程名</td>
			<td>
				<input id="cname" class="easyui-validatebox"/>
			</td>
			<td> 
				<a id="btn-search" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a> 
			</td>
		</tr>
		
	</table>
	<script type="text/javascript">
		$('#btn-search').click(function(){
			$('#dg').datagrid('load',{
				cname:$('#cname').val()
			})
		});
	</script>
	
	<!-- 工具条 -->
	<div id="toolbar">
		<a id="btn-remove" href="#" class="easyui-linkbutton"
			data-options="iconCls:'icon-remove'">删除</a>
		<a id="btn-marking" href="#" class="easyui-linkbutton" 
			data-options="iconCls:'icon-edit'">打分</a>
	</div>
	<!-- 数据表格 -->
	<table id="dg"></table>
	<!-- 添加对话框 -->
	<div id="add-dialog" class="easyui-dialog" title="添加学生信息"
		style="width: 400px; height: 200px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="add-form" method="post">
			<table>
				<tr>
					<td>课程名</td>
					<td><input type="text" name="cname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>课程描述</td>
					<td><input type="text" name="cdesc" class="easyui-validatebox" /></td>

				</tr>

			</table>

		</form>
	</div>
	
	<!-- 修改对话框 -->
	<div id="edit-dialog" class="easyui-dialog" title="修改学生信息"
		style="width: 400px; height: 200px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="edit-form" method="post">
			<table>
				<tr>
				<input type="hidden" name="cid"/>
					<td>课程名</td>
					<td><input type="text" name="cname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>课程描述</td>
					<td><input type="text" name="cdesc" class="easyui-validatebox" /></td>

				</tr>

			</table>

		</form>
	</div>

	<!-- 打分对话框 -->
	<div id="marking-dialog" class="easyui-dialog" title="登记成绩"
		style="width: 400px; height: 200px;"
		data-options="iconCls:'icon-edit',model:true,closed:true">
		<form id="marking-form" method="post">
			<table>
				
				<tr>
					<td>课程名</td>
					<td><input readonly type="text" name="cname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>学生名</td>
					<td><input readonly type="text" name="sname" class="easyui-validatebox" /></td>

				</tr>
				<tr>
					<td>分数</td>
					<td><input type="text" name="score" class="easyui-validatebox" /></td>
				
				</tr>
				<tr>
					<td><input type="hidden" name="cid"/>
				<input type="hidden" name="sid"/></td>
				</tr>
			</table>

		</form>
	</div>
	
</body>
</html>