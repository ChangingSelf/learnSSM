<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="../jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../jeasyui/themes/icon.css">
<script type="text/javascript" src="../jeasyui/jquery.min.js"></script>
<script type="text/javascript" src="../jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../jeasyui/easyui-lang-zh_CN.js"></script>
</head>
<body>
	数据表格 
	<script type="text/javascript">
	$(function(){
		//添加学生信息
		$('#btn-add').click(function(){
			$('#add-dialog').dialog({
				closed:false,
				buttons:[{
					text:'提交',
					iconCls:'icon-save',
					handler:function(){
						$('#add-form').form('submit', {   
						    url:'../student/insertStudent',   
						    onSubmit: function(){   
						        return $('#add-form').form('validate');
						    },   
						    success:function(flag){   
						        if(flag){
						        	$('#dg').datagrid('load');
						        	$('#add-dialog').dialog({
										closed:true
									});
						        }else{
						        	alert('操作失败');
						        }
						    }   
						});  
					}
				},{
					text:'重置',
					iconCls:'icon-redo',
					handler:function(){
						alert('reset');
					}
				}]
				
			})
			
		})
		//删除学生信息
		$('#btn-remove').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择要删除的数据');
			}else{
				const ids=[];
				
				for(var i in array){
					ids.push(array[i].sid);
				}
				console.log(ids);
				//jquery原生方法
				$.post('../student/deleteStudent',{
					'sids[]':ids
					},function(flag){
					if(flag){
						$('#dg').datagrid('reload');
					}else{
						alert('操作失败');
					}
				})
			}
		})
		//修改学生信息
		$('#btn-edit').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择要修改的数据');
			}else if(array.length!=1){
				alert('不能同时修改多条数据');
			}else{
				const student = $('#dg').datagrid('getSelected');
				console.log(student);
				$('#edit-form').form('load',{//加载本地记录
					sid:student.sid,
					sname:student.sname,
					snum:student.snum,
					sex:student.sex,
					age:student.age,
					address:student.address,
					gid:student.grade.gid
				});
				$('#edit-dialog').dialog({
					closed:false,
					buttons:[{
						text:'提交',
						iconCls:'icon-save',
						handler:function(){
							$('#edit-form').form('submit', {   
							    url:'../student/updateStudent',   
							    onSubmit: function(){   
							        return $('edit-form').form('validate');
							    },   
							    success:function(flag){   
							        if(flag){
							        	$('#dg').datagrid('load');
							        	$('#edit-dialog').dialog({
											closed:true
										});
							        }else{
							        	alert('操作失败');
							        }
							    }   
							});
						}
					}]
					
				});

			}
		});
		
		$('#dg').datagrid({
			url : '../student/listPageStudent',
			fit: true,
			fitColumns:true,
			rownumbers:true,
			striped:true,
			pagination:true,
			pagePosition:'both',
			toolbar:'toolbar',
			columns : [ [ {
				field : 'sid',
				title : '学号',
				width : 100,
				checkbox:true
			}, {
				field : 'sname',
				title : '姓名',
				width : 100
			}, {
				field : 'snum',
				title : '学号',
				width : 100
			}, {
				field : 'sex',
				title : '性别',
				width : 100
			}, {
				field : 'age',
				title : '年龄',
				width : 100
			}, {
				field : 'address',
				title : '地址',
				width : 100
			} , {
				field : 'grade',
				title : '班级',
				width : 100,
				formatter: function(grade,row,index){
					if (grade!=null){
						return grade.gname;
					} else {
						return '';
					}
				}

			} ] ]
		});

	})
	</script>
	
	<!-- 查询框 -->
	<table>
		<tr>
			<td>姓名</td>
			<td>
				<input id="sname" class="easyui-validatebox"/>
			</td>
			<td>性别</td>
			<td>
				<input id="sex" class="easyui-validatebox"/>
			</td>
			<td>
				<a id="btn-search" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a> 
			</td>
		</tr>
		
	</table>
	<script type="text/javascript">
		$('#btn-search').click(function(){
			$('#dg').datagrid('load',{
				sname:$('#sname').val(),
				sex:$('#sex').val()
			})
		});
	</script>
	
	<!-- 工具条 -->
	<div id="toolbar">
		<a id="btn-add" href="#" class="easyui-linkbutton"
			data-options="iconCls:'icon-add'">添加</a> 
		<a id="btn-edit" href="#" class="easyui-linkbutton" 
			data-options="iconCls:'icon-edit'">修改</a>
		<a id="btn-remove" href="#" class="easyui-linkbutton"
			data-options="iconCls:'icon-remove'">删除</a>
	</div>
	<!-- 数据表格 -->
	<table id="dg"></table>
	<!-- 添加对话框 -->
	<div id="add-dialog" class="easyui-dialog" title="添加学生信息"
		style="width: 400px; height: 300px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="add-form" method="post">
			<table>
				<tr>
					<td>姓名</td>
					<td><input type="text" name="sname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>学号</td>
					<td><input type="text" name="snum" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>性别</td>
					<td>
					<select class="easyui-combobox" name="sex" style="width:200px;">  
					    <option value="男">男</option>  
					    <option value="女">女</option>    
					</select>  </td>

				</tr>
				<tr>
					<td>年龄</td>
					<td><input type="text" name="age" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>地址</td>
					<td><input type="text" name="address" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>班级</td>
					<td><input class="easyui-combobox" name="gid"
						data-options="valueField:'gid',textField:'gname',url:'../grade/listGrade'" />
					</td>

				</tr>

			</table>

		</form>
	</div>
	
	<!-- 修改对话框 -->
	<div id="edit-dialog" class="easyui-dialog" title="修改学生信息"
		style="width: 400px; height: 300px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="edit-form" method="post">
			<table>
				<input type="hidden" name="sid"/>
				<tr>
					<td>姓名</td>
					<td><input type="text" name="sname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>学号</td>
					<td><input type="text" name="snum" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>性别</td>
					<td>
					<select class="easyui-combobox" name="sex" style="width:200px;">  
					    <option value="男">男</option>  
					    <option value="女">女</option>    
					</select>  </td>

				</tr>
				<tr>
					<td>年龄</td>
					<td><input type="text" name="age" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>地址</td>
					<td><input type="text" name="address" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>班级</td>
					<td><input class="easyui-combobox" name="gid"
						data-options="valueField:'gid',textField:'gname',url:'../grade/listGrade'" />
					</td>

				</tr>
			</table>

		</form>
	</div>

</body>
</html>