<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="../jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../jeasyui/themes/icon.css">
<script type="text/javascript" src="../jeasyui/jquery.min.js"></script>
<script type="text/javascript" src="../jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../jeasyui/easyui-lang-zh_CN.js"></script>
</head>
<body>
	数据表格
	<script type="text/javascript">
	$(function(){
		
		//选课
		$('#btn-select').click(function(){
			const array = $('#dg').datagrid('getSelections');
			if(array.length==0){
				alert('请选择要选择的课程');
			}else if(array.length!=1){
				alert('不能同时选择多门课程');
			}else{
				const course = $('#dg').datagrid('getSelected');
				console.log(course);
				$('#select-form').form('load',{//加载本地记录
					cid:course.cid,
					cname:course.cname,
					cdesc:course.cdesc
				});
				$('#select-dialog').dialog({
					closed:false,
					buttons:[{
						text:'提交',
						iconCls:'icon-save',
						handler:function(){
							$('#select-form').form('submit', {   
							    url:'../studentCourse/chooseCourse',   
							    onSubmit: function(){   
							        return $('select-form').form('validate');
							    },   
							    success:function(flag){   
							        if(flag){
							        	$('#dg').datagrid('load');
							        	$('#select-dialog').dialog({
											closed:true
										});
							        }else{
							        	alert('操作失败');
							        }
							    }   
							});
						}
					}]
					
				});

			}
		});
		
		
		$('#dg').datagrid({
			url : '../course/listPageCourse',
			fit: true,
			fitColumns:true,
			rownumbers:true,
			striped:true,
			pagination:true,
			pagePosition:'both',
			toolbar:'toolbar',
			columns : [ [ {
				field : 'cid',
				title : '课程号',
				width : 100,
				checkbox:true
			}, {
				field : 'cname',
				title : '课程名',
				width : 100
			}, {
				field : 'cdesc',
				title : '课程简介',
				width : 100
			} ] ]
		});

	})
	</script>
	
	<!-- 查询框 -->
	<table>
		<tr>
			<td>课程名</td>
			<td>
				<input id="cname" class="easyui-validatebox"/>
			</td>
			<td> 
				<a id="btn-search" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a> 
			</td>
		</tr>
		
	</table>
	<script type="text/javascript">
		$('#btn-search').click(function(){
			$('#dg').datagrid('load',{
				cname:$('#cname').val()
			})
		});
	</script>
	
	<!-- 工具条 -->
	<div id="toolbar">
		<a id="btn-select" href="#" class="easyui-linkbutton"
			data-options="iconCls:'icon-add'">选课</a> 
	</div>
	<!-- 数据表格 -->
	<table id="dg"></table>
	<!-- 选课对话框 -->
	<div id="select-dialog" class="easyui-dialog" title="选课"
		style="width: 400px; height: 300px;"
		data-options="iconCls:'icon-add',model:true,closed:true">
		<form id="select-form" method="post">
			<table>
			<input type="hidden" name="cid"/>
				<tr>
					<td>课程名</td>
					<td><input type="text" name="cname" class="easyui-validatebox"
						data-options="required:true" /></td>

				</tr>
				<tr>
					<td>课程描述</td>
					<td><input type="text" name="cdesc" class="easyui-validatebox" /></td>

				</tr>
				<tr>
					<td>选课学生</td>
					<td><input class="easyui-combobox" name="sid"
						data-options="valueField:'sid',textField:'sname',required:true,url:'../student/listStudent'" />
					</td>

				</tr>
			</table>

		</form>
	</div>
	
	

</body>
</html>